from fastapi import FastAPI, HTTPException, Body, Path, Query, Request, Depends
from fastapi.responses import HTMLResponse,JSONResponse
from pydantic import BaseModel, Field
from typing import Optional,List
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder

class JWTBeater(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != 'a@david.com':
            raise HTTPException(status_code=403, detail= 'Non valid credentials')
app = FastAPI()
app.title = 'Runing App'
app.version = '2.0.1'

Base.metadata.create_all(bind = engine)
class User(BaseModel):
    email: str 
    password:str


class Movie(BaseModel):
    id : Optional[int] = None
    # Se puede colocar un default por valor
    # default='Non Named'
    title : str = Field(min_length=5, max_length=15)
    overview : str = Field(min_length=5, max_length= 50)
    year : int = Field(le = 2022)
    rating : float = Field(ge = 1, le=10)
    category : str = Field(min_length=5, max_length=15)

    model_config = {
        'json_schema_extra' : {
            'example':{
                'id': 1,
                'title' : 'Non Named',
                'overview' : 'Non Overvier',
                'year': 2022,
                'rating' : 5.0,
                'category': 'Non Category'
                }
        }

    }

movies = [
    {
    'id': 1,
    'title': 'Avatar',
    'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
    'year': 2009,
    'rating': 7.8,
    'category': 'Acción'
    },  
    {
    'id': 2,
    'title': 'Cavatar',
    'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
    'year': 2010,
    'rating': 7.8,
    'category': 'Acción'
    }
]


@app.get('/', tags = ['home'])


def message():
    return HTMLResponse('<h1>Hello World!</h1>')

@app.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "a@david.com" and user.password == "admin":
        token : str = create_token(user.dict())
        return JSONResponse(status_code= 200, content = token)

@app.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBeater())])

def get_movies() -> List[Movie]:
    db = Session()
    result = db.query(MovieModel).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@app.get ('/movies/{id}', tags=['get_movies'], response_model=Movie)
def get_movie_id(id : int = Path(ge = 1, le = 2000)) -> Movie:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).all()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))    
    '''
    #Otra manera de crearlo sin un session:
    for item in movies:
        if item['id']  == id:
            return JSONResponse(content=item)
    raise HTTPException(status_code=404, 
    detail="Movie not found")'''

@app.get('/movies/', tags=['movies'], response_model=List[Movie])
def get_movies_by_category(category : str = Query(min_length=5, max_length=15) ) -> Movie:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.category == category).all()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))  
    '''data = [item for item in movies if item['category'] == category]
    return JSONResponse(content=data)'''
@app.get('/movies-all/{year}', tags=['movies'])
def get_movies_by_year(year : int = Path(le = 2023)):
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.year == year).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))  
    
    
    #return [item for item in movies if item['year'] == year]

@app.post('/movies', tags =['movies'], response_model= dict, status_code= 201)
def create_movies(movie : Movie) -> dict :
    db = Session()
    new_movie = MovieModel(**movie.dict())
    db.add(new_movie) 
    db.commit()
    return JSONResponse(status_code=201, content={'message':'Se ha creado la pelicula'})
    
    '''Se crea con la clase MOVIE
        movies.append({
        'id': id,
        'title':title,
        'overview':overview,
        'year': year,
        'rating': rating,
        'category': category
    })'''
    
@app.delete('/movies', tags =['movies'], response_model= dict, status_code= 200)
def remove_movies(id: int) -> dict:    
    for item in movies:
        if item["id"] == id:
            movies.remove(item)
            #return 'Hello mother'
    return JSONResponse(status_code=200, content={'message': 'Se ha eliminado la pelicula'}) 
@app.put('/movies/{id}', tags = ['movies'], response_model= dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    for item in movies:
        if item['id'] == id:
            item['title'] = movie.title
            item['year'] = movie.year
            item['overview'] = movie.overview
            item['rating'] = movie.rating
            item['category'] = movie.category
            return JSONResponse(status_code=200, content={'message': 'Se ha modificado la pelicula'})
    
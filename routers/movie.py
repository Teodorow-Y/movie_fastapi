from fastapi import APIRouter
from fastapi import FastAPI, HTTPException, Body, Path, Query, Request, Depends
from fastapi.responses import HTMLResponse,JSONResponse
from pydantic import BaseModel, Field
from typing import Optional,List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBeater
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBeater())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.get('/movies/', tags=['get_movies'], response_model=List[Movie])
def get_movies_by_category(category : str = Query(min_length=5, max_length=15) ) -> Movie:
    db = Session()
    result = MovieService(db).get_movie_category(category)
    # Esto se ha movido al services:  db.query(MovieModel).filter(MovieModel.category == category).all()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))  
    '''data = [item for item in movies if item['category'] == category]
    return JSONResponse(content=data)'''

@movie_router.get ('/movies/{id}', tags=['get_movies'], response_model=Movie)
def get_movie_by_id(id : int = Path(ge = 1, le = 2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie_id(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))    
    '''
    #Otra manera de crearlo sin un session:
    for item in movies:
        if item['id']  == id:
            return JSONResponse(content=item)
    raise HTTPException(status_code=404, 
    detail="Movie not found")'''


@movie_router.get('/movies/{year}', tags=['get_movies'])
def get_movies_by_year(year : int = Path(le = 2023)):
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.year == year).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))  
    
    
    #return [item for item in movies if item['year'] == year]

@movie_router.post('/movies', tags =['movies'], response_model= dict, status_code= 201)
def create_movies(movie : Movie) -> dict :
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={'message':'Se ha creado la pelicula'})
    
    '''Se crea con la clase MOVIE
        movies.append({
        'id': id,
        'title':title,
        'overview':overview,
        'year': year,
        'rating': rating,
        'category': category
    })'''
    
@movie_router.delete('/movies', tags =['movies'], response_model= dict, status_code= 200)
def remove_movies(id: int) -> dict: 
    db = Session()
    #result = db.query(MovieModel).filter(MovieModel.id == id).first()
    result : MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse( status_code=404, content={'message': 'No encontrado'})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={'message': 'Se ha eliminado la pelicula'})    
    '''db.delete(result)
    db.commit()'''
    '''for item in movies:
        if item["id"] == id:
            movies.remove(item)
            #return 'Hello mother'''
    
@movie_router.put('/movies/{id}', tags = ['movies'], response_model= dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie_id(id)
    #db.query(MovieModel).filter(MovieModel.id == id).first()
    
    if not result:
        return JSONResponse( status_code=404, content={'message': 'No encontrado'})
    MovieService(db).update_movie(id, movie)

    return JSONResponse(status_code=200, content={'message': 'Se ha modificado la pelicula'})
    
    '''result.title = movie.title
    result.overview = movie.overview
    result.year = movie.year
    result.rating = movie.rating
    result.category = movie.category
    db.commit()'''

    '''for item in movies:
        if item['id'] == id:
            item['title'] = movie.title
            item['year'] = movie.year
            item['overview'] = movie.overview
            item['rating'] = movie.rating
            item['category'] = movie.category'''
    